//VARIABLES GLOBALES
var pacientes_n;
var camas = [];

//functions

function get_index_fromid(id){
     let camas_index = camas.length;
     let found_index;
     for (let i = 0; i < camas_index; i++){
         console.log(i);
         if (camas[i].id === id){
         found_index = i;
         } else {}
     }
     return found_index;
}

function save_camas(){
    let save1 = JSON.stringify(camas);
    localStorage.setItem("stored_camas", save1);
}


function onload(){

    //SPLASH BEGIN----------------------------------------------------------------------
    var load_splash = sessionStorage.getItem("splash");
    var load_splash2 = JSON.parse(load_splash);
    if (load_splash2 === true){
        splash = load_splash2;
    } else if (load_splash2 === false){
        splash = load_splash2; 
    } else if (load_splash2 === null || load_splash2 === undefined){
        splash = false;
    }

  if (splash === false){
    setTimeout(function(){ 
        $("#splash").hide("fast"); 
       
      
    }, 1000);

    setTimeout(function(){ 
     
        $("#dropdownMenu3").show("fast");
     
    }, 300);

    setTimeout(function(){ 
   

        $(navbar_p_cama).show("fast");
    }, 600);
  } else if (splash === true) {
    console.log("no animation");
    $("#splash").hide();
    $("#dropdownMenu3").show();
    $(navbar_p_cama).show();
  }

  splash = true;
  
  sessionStorage.setItem("splash", splash);

  console.log( "SPLASH IS " + splash);
   //SPLASH END --------------------------------------------------------------

   //LOAD CAMAS OBJECT FROM LOCALSTORAGE
   
   let load_camas = localStorage.getItem("stored_camas");
   if (!load_camas){
       console.log("no camas data");
   } else {
    let load_camas2 = JSON.parse(load_camas);
    camas = load_camas2
   }


   pacientes_n = camas.length;
   camas_length = camas.length;
   
   //PUT CAMAS TO FRONTEND
   let cant = 0;
   camas.forEach((camas)=>{

    cant++
    console.log(`${camas.numero}${camas.nombre}${camas.edad}`);
    $("#camas_data").append(`<div class="form-row" id="mainpac_${camas.numero}${camas.nombre}${camas.edad}">
    <div class="btn-group">

   
    <button class="btn btn-danger delete_pac" id="delete_${camas.id}">x</button>
    <button class="btn btn-dark change_cama" id="_${camas.numero}${camas.nombre}${camas.edad}">${camas.numero}</button>
    <button class="btn btn-dark change_nombre"  id="_${camas.numero}${camas.nombre}${camas.edad}">${camas.nombre}</button>
    <button class="btn btn-dark change_edad"  id="_${camas.numero}${camas.nombre}${camas.edad}">${camas.edad}</button>
    <button class="btn btn-dark ver_but" id="ver_${camas.numero}${camas.nombre}${camas.edad}"><img src="./images/notepad.png" height="20px"></button>

    </div>
    </div>

    <div style="display:none;" class="card bg-light text-primary cont_container controles_div${cant}" id="controles_${camas.numero}${camas.nombre}${camas.edad}">
    
    <div class="card-header bg-primary text-white">
    <h3>Pendientes</h3>
    <button class="btn btn-sm btn-block btn-primary espend_but" >+</button>
    <div class="estudios" id="_${camas.numero}${camas.nombre}${camas.edad}"></div>
    </div>
    
    <div class="card-header bg-primary text-white">
    <h3 id="${camas.numero}${camas.nombre}${camas.edad}">Controles <img src="./images/notepad.png" height="50px"></h3>
    

    <div class="balcenter_buts " id="${camas.numero}${camas.nombre}${camas.edad}">
        <h3 class="text-left"></h3>
        <button class="btn btn-dark btn-sm hemodinamico maincont_buttons" id="addhem_${camas.numero}${camas.nombre}${camas.edad}">Hemodinamico <img src="./images/gauge.png" height="20px"></button>
        <button class="btn btn-dark btn-sm respiratorio maincont_buttons" id="addres_${camas.numero}${camas.nombre}${camas.edad}">Respiratorio <img src="./images/lungs-silhouette.png" height="20px"></button>
        <button class="btn btn-dark btn-sm balance maincont_buttons" id="addbal_${camas.numero}${camas.nombre}${camas.edad}">Balance <img src="./images/arrows.png" height="20px"></button>
        <button class="btn btn-dark btn-sm ritdiu maincont_buttons" id="addrit_${camas.numero}${camas.nombre}${camas.edad}">Rit. Diuretico <img src="./images/kidneys.png" height="20px"></button>
        <button class="btn btn-dark btn-sm hgt maincont_buttons" id="addhgt_${camas.numero}${camas.nombre}${camas.edad}">HGT <img src="./images/restaurant.png" height="20px"></button>
    </div>
    </div>
    <br>
    </div><br id="br_${camas.numero}${camas.nombre}${camas.edad}">`);
    let controles_length = camas.controles.length; 
    let pendientes_length = camas.pendientes.length;
                    
    for (let m = 1; m <= camas_length; m++){
        console.log("m is: " + m);    
        let get_maindiv_id = $(".controles_div" + m).attr("id");
       
        //PENDIENTES=========================================================================================
        for(let e = 1; e <= pendientes_length; e++){
          
            

            if ("controles_" + camas.numero + camas.nombre + camas.edad === get_maindiv_id){
                
                console.log("append pendiente");
                $(".estudios#" + camas.pendientes[+e-1].id).append(`
                <br id="${e}_${camas.pendientes[+e-1].id}">
                <div class="card bg-primary text-white hemodinamico ${camas.pendientes[+e-1].status} pendiente-card" id="pendientes_${e}_${camas.pendientes[+e-1].id}">
                    <div class="card-header">  
                    <button class="btn btn-sm btn-danger float-left delete_pendiente">x</button>                   
                    <h6>${camas.pendientes[+e-1].titulo}</h6>
                    <hr>            
                    <button class="btn btn-sm btn-primary pendiente">Pendiente</button>
                    <button class="btn btn-sm btn-primary encurso">En Curso</button>
                    <button class="btn btn-sm btn-primary hecho">Hecho</button>
                    </div>                                           
                </div>          
                `)
            } else {
                console.log("do not append pendiente");
            }

        }


        //CONTROLES===========================================================================================
        for (let c = 1; c <= controles_length; c++){
         
            if (camas.controles[+c-1].type === "hemodinamico"){                                                                                    
                console.log("controles_div id is: " + get_maindiv_id);
                console.log("boton id is: " + camas.numero + camas.nombre + camas.edad);
                if ("controles_" + camas.numero + camas.nombre + camas.edad === get_maindiv_id){
                    //format date
                    let c_date_string = camas.controles[+c-1].date;
                    let c_raw_date = new Date(c_date_string);
                    let hs = c_raw_date.getHours();
                    let min = c_raw_date.getMinutes();
                    if (min < 10 ){
                        _min = "0" + JSON.stringify(min);
                        min = _min;
                        console.log("n_min is: " + _min);
                    } else if (min > 10){
                        console.log("minutes is more than 10");                            
                    }
                    let month = c_raw_date.getMonth() + 1;
                    let day = c_raw_date.getDate();
                    let p_date = hs + ":" + min + " | " + day + "/" + month;
                    console.log(hs + ":" + min + " | " + day + "/" + month);
                   
                    
                    console.log("**append Hemodinamico**");
                    $(".controles_div" + m).append(`
                    <div class="card bg-primary text-white hemodinamico" style="min-width: 140px;" id="cont_hem_${c}_${camas.numero}${camas.nombre}${camas.edad}">
                        
                        <div class="card-header">
                        <h5>Control Hemodinamico</h5>
                        <p class="contr_date">${p_date}</p>
                        </div>                              
             
                        <button class="btn btn-sm btn-primary tasbut" id="tasbut_${c}_${camas.numero}${camas.nombre}${camas.edad}">TAS:${camas.controles[+c-1].tas}</button>
                        <button class="btn btn-sm btn-primary tadbut" id="tadbut_${c}_${camas.numero}${camas.nombre}${camas.edad}">TAD:${camas.controles[+c-1].tad}</button>
                        <button disabled class="btn btn-sm btn-dark tambut" id="tambut_${c}_${camas.numero}${camas.nombre}${camas.edad}">TAM:${camas.controles[+c-1].tam}</button>                             
                    </div><br>`);
                } else {
                    console.log("id doesnt match");
                }                                
            } else{
                console.log("no control hemodinamico");
            }    
            
            if (camas.controles[+c-1].type === "respiratorio"){                               
                console.log("**append Respiratorio**");
                let c_date_string = camas.controles[+c-1].date;
                let c_raw_date = new Date(c_date_string);
                let hs = c_raw_date.getHours();
                let min = c_raw_date.getMinutes();
                if (min < 10 ){
                    _min = "0" + JSON.stringify(min);
                    min = _min;
                    console.log("n_min is: " + _min);
                } else if (min > 10){
                    console.log("minutes is more than 10");                            
                }
                let month = c_raw_date.getMonth() + 1;
                let day = c_raw_date.getDate();
                let p_date = hs + ":" + min + " | " + day + "/" + month;
                console.log(hs + ":" + min + " | " + day + "/" + month);

                let get_maindiv_id = $(".controles_div" + m).attr("id"); 
                if ("controles_" + camas.numero + camas.nombre + camas.edad === get_maindiv_id){
                $(".controles_div" + m).append(`
                <div class="card bg-primary text-white border-secondary respiratorio " id="cont_res_${c}_${camas.numero}${camas.nombre}${camas.edad}">
                    <div class="card-header">
                    <h5 class="text-white">Control Respiratorio</h5>
                    <p class="contr_date">${p_date}</p>
                    </div>
                   
                    <button class="btn btn-sm btn-primary frbut" id="frbut_${c}_${camas.numero}${camas.nombre}${camas.edad}">FR:${camas.controles[+c-1].fr}</button>
                    <button class="btn btn-sm btn-primary satbut" id="satbut_${c}_${camas.numero}${camas.nombre}${camas.edad}">SAT:${camas.controles[+c-1].sat}</button>
                </div><br>`);   
                } else {
                    console.log("do not append respiratorio");
                }                           
            }

            if (camas.controles[+c-1].type === "balance"){

                //check if matching ids before appending
                let get_maindiv_id = $(".controles_div" + m).attr("id");                                
                let main_id = `${get_maindiv_id}`;
           
                console.log(main_id);
                let id = camas.id;      
                console.log(id);                      
         
                let c_date_string = camas.controles[+c-1].date;
                let c_raw_date = new Date(c_date_string);
                console.log("DATE: " + c_raw_date);
                let hs = c_raw_date.getHours();
                let min = c_raw_date.getMinutes();
                if (min < 10 ){
                    _min = "0" + JSON.stringify(min);
                    min = _min;
                    console.log("n_min is: " + _min);
                } else if (min > 10){
                    console.log("minutes is more than 10");                            
                }
                let month = c_raw_date.getMonth() + 1;
                let day = c_raw_date.getDate();
                console.log("day: " + day);
                let p_date = hs + ":" + min + " | " + day + "/" + month;
                console.log(hs + ":" + min + " | " + day + "/" + month);

                
                if (main_id === "controles" + id){
                console.log("**append Balance");   
                //hide balance button
                $("#addbal" + id).hide();                                                   
                $(".controles_div" + m).append(`
                <div class="card bg-primary text-white border-secondary balance" id="cont_bal_${c}_${camas.numero}${camas.nombre}${camas.edad}">              
                    <div class="card-header">
                    <h5 class="text-white">Balance</h5>
                    <p class="contr_date">${p_date}</p>                                                              
                    </div>
                                                         
                    <button class="btn btn-primary peso" value="${camas.controles[+c-1].peso}" id="bal_pes_${c}_${camas.numero}${camas.nombre}${camas.edad}">Peso ${camas.controles[+c-1].peso} Kg</button>
                    <br>
                    <div class="card-header">
                    <h5 class="text-white">Ingresos</h5>   
                    </div>                                                                    
                    <h6>VO</h6>
                  
                    <h6 class="ivo_val" id="bal_ivo_${c}_${camas.numero}${camas.nombre}${camas.edad}">${camas.controles[+c-1].ivo}</h6>
                    <div class="btn-group balcenter_buts">
                    <button class="btn btn-sm btn-primary ivo50" id="bal_ivo_50_${c}_${camas.numero}${camas.nombre}${camas.edad}">50</button>
                    <button class="btn btn-sm btn-primary ivo200" id="bal_ivo_200_${c}_${camas.numero}${camas.nombre}${camas.edad}">200</button>
                    <button class="btn btn-sm btn-primary ivo500" id="bal_ivo_500_${c}_${camas.numero}${camas.nombre}${camas.edad}">500</button>
                    </div>
                    <br>
                   
                    <h6>EV</h6>
                    <h6 id="bal_iev_${c}_${camas.numero}${camas.nombre}${camas.edad}">${camas.controles[+c-1].iev}</h6>
                    <div class="btn-group balcenter_buts">
                    <button class="btn btn-sm btn-primary iev50" id="bal_iev_50_${c}_${camas.numero}${camas.nombre}${camas.edad}">50</button>
                    <button class="btn btn-sm btn-primary iev200" id="bal_iev_200_${c}_${camas.numero}${camas.nombre}${camas.edad}">200</button>
                    <button class="btn btn-sm btn-primary iev500" id="bal_iev_500_${c}_${camas.numero}${camas.nombre}${camas.edad}">500</button>
                    </div>
                    <br>
                    <div class="card-header">
                    <h5>Egresos</h5>         
                    </div>                         
                    <h6>Diuresis</h6>
                    <h6 id="bal_diu_${c}_${camas.numero}${camas.nombre}${camas.edad}">${camas.controles[+c-1].diu}</h6>
                    <div class="btn-group balcenter_buts">
                    <button class="btn btn-sm btn-primary diu50" id="bal_diu_50_${c}_${camas.numero}${camas.nombre}${camas.edad}">50</button>
                    <button class="btn btn-sm btn-primary diu200" id="bal_diu_200_${c}_${camas.numero}${camas.nombre}${camas.edad}">200</button>
                    <button class="btn btn-sm btn-primary diu500" id="bal_diu_500_${c}_${camas.numero}${camas.nombre}${camas.edad}">500</button>
                    </div>
                    <br>
                   
                    <h6>Catarsis</h6>
                    <h6 id="bal_cat_${c}_${camas.numero}${camas.nombre}${camas.edad}">${camas.controles[+c-1].cat}</h6>
                    <div class="btn-group balcenter_buts">
                    <button class="btn btn-sm btn-primary catsum" id="bal_cat_sum_${c}_${camas.numero}${camas.nombre}${camas.edad}">+1</button>
                    <button class="btn btn-sm btn-primary catrest" id="bal_cat_res_${c}_${camas.numero}${camas.nombre}${camas.edad}">-1</button>                            
                    </div> 
                    <br>
                    <button class="btn btn-block btn-sm btn-dark" id="bal_main_${c}_${camas.numero}${camas.nombre}${camas.edad}" disabled >${camas.controles[+c-1].bal}</button>     
                    <button class="btn btn-sm btn-primary float-right save_balance" id="bal_save_${c}_${camas.numero}${camas.nombre}${camas.edad}">Calcular</button>                              
                </div><br>`);
                } else {
                    console.log("dont append balance");
                }
            }

            if (camas.controles[+c-1].type === "hgt"){
                //check if matching ids before appending
                let get_maindiv_id = $(".controles_div" + m).attr("id");                                
                let main_id = `${get_maindiv_id}`;
                let id = camas.id;
                
                console.log(main_id);
                console.log(id);

                let c_date_string = camas.controles[+c-1].date;
                let c_raw_date = new Date(c_date_string);
                console.log("DATE: " + c_raw_date);
                let hs = c_raw_date.getHours();
                let min = c_raw_date.getMinutes();
                if (min < 10 ){
                    _min = "0" + JSON.stringify(min);
                    min = _min;
                    console.log("n_min is: " + _min);
                } else if (min > 10){
                    console.log("minutes is more than 10");                            
                }
                let month = c_raw_date.getMonth() + 1;
                let day = c_raw_date.getDate();
                console.log("day: " + day);
                let p_date = hs + ":" + min + " | " + day + "/" + month;
                console.log(hs + ":" + min + " | " + day + "/" + month);

                if (main_id === "controles" + id){
                    $("#addhgt" + id).hide();
                    $(".controles_div" + m).append(`
                    <div class="card bg-primary border-secondary text-white">
                    <div class="card-header">                                                       
                    <h5>HGT</h5>
                    <p>${p_date}</p>
                    </div>
                    <div class="btn-group balcenter_buts">
                    <button class="btn btn-primary hgt_d" id="hgt_d_${c}_${camas.numero}${camas.nombre}${camas.edad}">D: ${camas.controles[+c-1].pd}</button>
                    <button class="btn btn-primary hgt_a" id="hgt_a_${c}_${camas.numero}${camas.nombre}${camas.edad}">A: ${camas.controles[+c-1].pa}</button>
                    <button class="btn btn-primary hgt_m" id="hgt_m_${c}_${camas.numero}${camas.nombre}${camas.edad}">M: ${camas.controles[+c-1].pm}</button>
                    <button class="btn btn-primary hgt_c" id="hgt_c_${c}_${camas.numero}${camas.nombre}${camas.edad}">C: ${camas.controles[+c-1].pc}</button>
                    </div>
                    </div><br>`)
                }

            }

            if (camas.controles[+c-1].type === "ritdiu"){
                //check if matching ids before appending
                let get_maindiv_id = $(".controles_div" + m).attr("id");                                
                let main_id = `${get_maindiv_id}`;
                let id = camas.id; 

                let c_date_string = camas.controles[+c-1].date;
                let c_raw_date = new Date(c_date_string);
                console.log("DATE: " + c_raw_date);
                let hs = c_raw_date.getHours();
                let min = c_raw_date.getMinutes();
                if (min < 10 ){
                    _min = "0" + JSON.stringify(min);
                    min = _min;
                    console.log("n_min is: " + _min);
                } else if (min > 10){
                    console.log("minutes is more than 10");                            
                }
                let month = c_raw_date.getMonth() + 1;
                let day = c_raw_date.getDate();
                console.log("day: " + day);
                let p_date = hs + ":" + min + " | " + day + "/" + month;
                console.log(hs + ":" + min + " | " + day + "/" + month);

                let pre_formatdate = camas.controles[+c-1].init_date;
                let date_object = new Date(pre_formatdate);
                console.log(date_object);
                let _day = date_object.getDate();
                let _month = date_object.getMonth() + 1;
                let _hs = date_object.getHours();
                let _mins = date_object.getMinutes();
                let _formatted_date = _day + "/" + _month;

                let _disable = "";
                let _hide = "";

                //disable hs and min and hide hoy ayer
                if (pre_formatdate === 0 || pre_formatdate === "0"){

                } else {
                    _disable = "disabled";
                    _hide = `style="display: none;"`;
                }

                if (main_id === "controles" + id){
                    $("#addritdiu" + id).hide();
                    $(".controles_div" + m).append(`
                    <div class="card bg-primary text-white border-secondary ritdiu" id="ritdiu_${c}_${camas.numero}${camas.nombre}${camas.edad}">
                    <div class="card-header">
                    <h5>Ritmo Diuretico</h5>
                    <p>${p_date}</p>
                    </div>
                   
                    <div class="card-header">
                    <h5>Inicio de Seguimiento</h6>
                    </div>
                    <h6>Hora y Dia de inicio</h6>
                    <div class="btn-group balcenter_buts">
                    <button class="btn btn-sm btn-primary ritdiu_hs" style="min-width: 50px;" ${_disable}>${_hs}</button>
                    <button class="btn btn-sm btn-dark" disabled>:</button>
                    <button class="btn btn-sm btn-primary ritdiu_mins" style="min-width: 50px;"  ${_disable}>${_mins}</button>
                    </div>
                    <br>
                    <p id="ritdiu_init_date">${_formatted_date}</p>
                    <div class="btn-group balcenter_buts" ${_hide}>
                    <button class="btn btn-sm btn-primary ritdiu_hoy" id="ritdiu_${c}_${camas.numero}${camas.nombre}${camas.edad}">Hoy</button>
                    <button class="btn btn-sm btn-primary ritdiu_ayer" id="ritdiu_${c}_${camas.numero}${camas.nombre}${camas.edad}">Ayer</button>
                    </div>
                    <br>
                    <h6>Diuresis de inicio en ml</h6>
                    <button class="btn btn-primary btn-sm ritdiu_initdiu" id="ritdiu_${c}_${camas.numero}${camas.nombre}${camas.edad}">${camas.controles[+c-1].init_diu}</button>
                    <div class="card-header">
                    <h5>Ingresar Diuresis</h5>
                    </div>
                    <p id="ritdiu_${c}_${camas.numero}${camas.nombre}${camas.edad}" value="${camas.controles[+c-1].current_diu}" class="ritdiu_newdiu">${camas.controles[+c-1].current_diu}</p>
                    <div class="btn-group balcenter_buts">
                    <button class="btn btn-sm btn-primary ritdiu_50" id="ritdiu_${c}_${camas.numero}${camas.nombre}${camas.edad}">+50</button>
                    <button class="btn btn-sm btn-primary ritdiu_200" id="ritdiu_${c}_${camas.numero}${camas.nombre}${camas.edad}">+200</button>
                    <button class="btn btn-sm btn-primary ritdiu_500" id="ritdiu_${c}_${camas.numero}${camas.nombre}${camas.edad}">+500</button>
                    </div>
                    <br> 
                    <button class="btn btn-block btn-sm btn-dark ritdiu_final" disabled>ml/hs</button>
                    <button class="btn btn-block btn-primary ritdiu_calc" id="ritdiu_${c}_${camas.numero}${camas.nombre}${camas.edad}">Calcular</button>                              
                    </div>
                    <br>
                    `)
                }
            }
        }
    }
   });

}


//AGREGAR PACIENTE -------------------------------------------------------------
$("#add_paciente").click(function(){
    let _newcama = $("#new_pac_cama").val();
    let newcama = _newcama.replace(/<script>/g,"");
    let _newnombre = $("#new_pac_nombre").val();
    let newnombre = _newnombre.replace(/<script>/g,"");
    let _newedad = $("#new_pac_edad").val();
    let newedad = _newedad.replace(/<script>/g,"");
    let newdate = new Date();
    let pacientes_div = $("#camas_data");
    let loadedby;

    let comp_id = newcama + newnombre + newedad;

    if (newnombre.length <= 3){

        pacientes_n++

    $(pacientes_div).append(`<div class="form-row" id="mainpac_${newcama}${newnombre}${newedad}">
    <div class="btn-group"> 

    <button class="btn btn-danger delete_pac" id="delete__${newcama}${newnombre}${newedad}">x</button>
    <button class="btn btn-dark change_cama" id="_${newcama}${newnombre}${newedad}">${newcama}</button>
    <button class="btn btn-dark change_nombre" id="_${newcama}${newnombre}${newedad}">${newnombre}</button>
    <button class="btn btn-dark change_edad" id="_${newcama}${newnombre}${newedad}">${newedad}</button>
    <button class="btn btn-dark ver_but" id="ver_${newcama}${newnombre}${newedad}"><img src="./images/notepad.png" height="20px"></button>
    </div>
    
    </div>
    <br id="br_${newcama}${newnombre}${newedad}">
    <div style="display:none; border-top-left-radius:0px; border-top-right-radius:0px;" class="card bg-light text-primary cont_container controles_div" id="controles_${newcama}${newnombre}${newedad}">
    
    <div class="card-header bg-primary text-white">
    <h3>Pendientes</h3>
    <button class="btn btn-sm btn-block btn-primary espend_but" >+</button>
    <div class="estudios" id="_${newcama}${newnombre}${newedad}"></div>
    </div>  

    <div class="card-header bg-primary text-white">
    <h3 id="${newcama}${newnombre}${newedad}">Controles <img src="./images/notepad.png" height="50px"></h3>
    <div class="balcenter_buts" id="${newcama}${newnombre}${newedad}">
        <h3 class="text-left"></h3>
        <button class="btn btn-dark btn-sm hemodinamico maincont_buttons" id="addhem_${newcama}${newnombre}${newedad}">Hemodinamico <img src="./images/gauge.png" height="20px"></button>
        <button class="btn btn-dark btn-sm respiratorio maincont_buttons" id="addres_${newcama}${newnombre}${newedad}">Respiratorio <img src="./images/lungs-silhouette.png" height="20px"></button>
        <button class="btn btn-dark btn-sm balance maincont_buttons" id="addbal_${newcama}${newnombre}${newedad}">Balance <img src="./images/arrows.png" height="20px"></button>
        <button class="btn btn-dark btn-sm ritdiu maincont_buttons" id="addrit_${newcama}${newnombre}${newedad}">Rit. Diuretico <img src="./images/kidneys.png" height="20px"></button>
        <button class="btn btn-dark btn-sm hgt maincont_buttons" id="addhgt_${newcama}${newnombre}${newedad}">HGT <img src="./images/restaurant.png" height="20px"></button>
    </div>
    </div>
    <hr>
    </div>
    </div>`);

    //store data to localstorage here
    let new_cama = {
        numero: newcama,
        nombre: newnombre,
        edad: newedad, 
        pendientes: [],
        id: `_${newcama}${newnombre}${newedad}`,
        controles: []
    }

    camas.push(new_cama);

    //save to localstorage
    let save1 = JSON.stringify(camas);
    localStorage.setItem("stored_camas", save1);
  
    $("#new_pac_cama").val("");
    $("#new_pac_nombre").val("");
    $("#new_pac_edad").val("");
    

    } else {
        alert("Usa solo 3 letras para identificar al paciente");
    }

    
});

//-----------------------------------------------------------------------------------------------------
//PRESS BUT CONTROL HEMODINAMICO
//------------------------------------------------------------------------------------------------------
$("body").on("click", "button.hemodinamico", function(){
    let id_raw = $(this).attr("id");
    //let id = id_raw.substr(6);
    let main_d = $(this).parent();    
    let grabbed_id = main_d.attr("id");
    console.log("grabbed id is: " + grabbed_id);
    let id = "_" + grabbed_id;
    console.log(id);
    let date = new Date();
    //console.log(id);

    //date
    let hs = date.getHours();
    let min = date.getMinutes();
    let _min;
    //console.log("minutes: " + min);
    if (min < 10 ){
        _min = "0" + JSON.stringify(min);
        min = _min;
        //console.log("n_min is: " + _min);
    } else if (min > 10){
        //console.log("minutes is more than 10");
    }
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let p_date = hs + ":" + min + " | " + day + "/" + month;

    //get pos
    let a = main_d.parent().parent().find(".card");
    //console.log("length: " + a.length);
    let b = +a.length + 1;
    //console.log(b);
    pos = b;

    $(this).parent().parent().parent().append(`
    <div class="card bg-primary text-white hemodinamico border-secondary" id="cont_hem_${pos}${id}">
        <div class="card-header">                     
        <h5>Control Hemodinamico</h5>
        <p class="contr_date">${p_date}</p>
        </div>             
        <button class="btn btn-sm btn-primary tasbut" value="0" id="tasbut_${pos}${id}">TAS:</button>
        <button class="btn btn-sm btn-primary tadbut" value="0" id="tadbut_${pos}${id}">TAD:</button>
        <button disabled class="btn btn-sm btn-dark tambut" value="0" id="tambut_${pos}${id}">TAM:</button>                             
    </div><br>
    `);

    //save to camas and localstorage

    let found_index = get_index_fromid(id);


    console.log("found index is: " + found_index);

    //modify camas and save to localstorage

    camas[found_index].controles.push({type: "hemodinamico", date: date, tas: 0, tad: 0, tam: 0, id: id})

    let save1 = JSON.stringify(camas);
    localStorage.setItem("stored_camas", save1);
  

});
//PRESS BUT TAS
$("body").on("click", "button.tasbut", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(8);
    let pos = id_raw.substring(7, 8);
    console.log(id_raw);
    console.log(id);
    console.log(pos);
    let tad_n;
    console.log("POS: " + pos);

    let tasval = prompt("Ingresar TAS");
    $(this).html("TAS: " + tasval);
    $(this).attr("value", tasval);


    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].tas = tasval;

    save_camas();

    //local tam calc
    var tad_html = $("#tadbut_" + pos + id).html();
    console.log(tad_html);
    if (tad_html === undefined){
        tad_html = $("#tadbut_" + "x" + id).val();
        console.log(tad_html + " :value");
        tad_n = tad_html;
    } else {
        tad_n = tad_html.substr(4);
    }

    let one = 2 * +tad_n;
    let two = +one + +tasval;
    let result_ = two / 3;
    let result = result_.toFixed(0);

    //get tam button
    let tam = $(this).parent().find(".tambut").html();
    console.log(tam + " tam");
    if ( tam === undefined){
        $(this).parent().find(".tambut").html("TAM:" + result);
    } else {
        $(this).parent().find(".tambut").html("TAM:" + result);
    }

});
//PRESS BUT TAD
$("body").on("click", "button.tadbut", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(8);
    let pos = id_raw.substring(7, 8);
    console.log(id_raw);
    console.log(id);
    console.log(pos);
    let tas_n;


    let tadval = prompt("Ingresar TAD");
    $(this).html("TAD: " + tadval);

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].tad = tadval;

    save_camas();

    //local tam calc
    var tas_html = $("#tasbut_" + pos + id).html();
    console.log(tas_html);
    if (tas_html === undefined){
        tas_html = $("#tasbut_" + "x" + id).val();
        console.log(tas_html + " :value");
        tas_n = tas_html;
    } else {
        tas_n = tas_html.substr(4);
    }

    let one = 2 * +tadval;
    console.log(one);
    let two = +one + +tas_n;
    console.log(two);
    let result_ = two / 3;
    
    let result = result_.toFixed(0);
    console.log(result);

    //get tam button
    let tam = $(this).parent().find(".tambut").html();
    console.log(tam + " tam");
    if ( tam === undefined){
        $(this).parent().find(".tambut").html("TAM:" + result);
    } else {
        $(this).parent().find(".tambut").html("TAM:" + result);
    }
   
});

//-----------------------------------------------------------------------------------------------------
//PRESS BUT CONTROL RESPIRATORIO
//------------------------------------------------------------------------------------------------------

$("body").on("click", "button.respiratorio", function(){
    let id_raw = $(this).attr("id");
    console.log(id_raw);
    let id = id_raw.substr(6);
    console.log(id);
    let date = new Date();
    console.log(id);

    //date
    let hs = date.getHours();
    let min = date.getMinutes();
    if (min < 10 ){
        _min = "0" + JSON.stringify(min);
        min = _min;
        console.log("n_min is: " + _min);
    } else if (min > 10){
        console.log("minutes is more than 10");                            
    }
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let p_date = hs + ":" + min + " | " + day + "/" + month;

    let fetch_raw_id = $(this).parent().attr("id");
    console.log("fetch raw id: " + fetch_raw_id);

    let pac_id = fetch_raw_id.substr(10);
    console.log("pac id: " + pac_id);
    
    //get pos
    let main_d = $(this).parent().parent().parent();    
    let a = main_d.find(".card");
    console.log("length: " + a.length);
    let b = +a.length + 1;
    console.log(b);
    pos = b;
   

    $(this).parent().parent().parent().append(`
    <div class="card bg-primary text-white border-secondary respiratorio " id="cont_res_${pos}_${pac_id}">
        <div class="card-header">
        <h5>Control Respiratorio</h5>
        <p class="contr_date">${p_date}</p>
        </div>                            
        <button class="btn btn-sm btn-primary frbut" id="frbut_${pos}${id}">FR:</button>
        <button class="btn btn-sm btn-primary satbut" id="satbut_${pos}${id}">SAT:</button>
    </div><br>
    `);

    let found_index = get_index_fromid(id);

    camas[found_index].controles.push({type: "respiratorio", date: date, fr: 0, sat: 0, loadedby: "", id: id});

    save_camas();
});

//PRESS BUT FR
$("body").on("click", "button.frbut", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(7);
    let pos = id_raw.substring(6, 7);
    console.log(id_raw);
    console.log(id);
    console.log(pos);

    console.log("POS is: " + pos);

    let frval = prompt("Ingresar FR");
    $(this).html("FR: " + frval);


    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].fr = frval;

    save_camas();

    //get tam button
    $("#frbut_" + pos + id).html("FR:" + frval);
});

//PRESS BUT SAT
$("body").on("click", "button.satbut", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(8);
    let pos = id_raw.substring(7, 8);
    console.log(id_raw);
    console.log(id);
    console.log(pos);

    // POS check
    if (pos > 0){
        console.log("pos is a number");
        console.log("pos is: " + pos);
    } else {
        console.log("pos is not a number");
        let main_d = $(this).parent().parent();    
        let a = main_d.find(".form-control");
        let b = a.length ;
        console.log(b);
        pos = b + 1;
    }

    console.log("POS is: " + pos);

    let satval = prompt("Ingresar Sat");
    $(this).html("Sat: " + satval);

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].sat = satval;

    save_camas();

    //get tam button
    $("#satbut_" + pos + id).html("Sat:" + satval);
});

//-----------------------------------------------------------------------------------------------------
//PRESS BUT CONTROL BALANCE
//------------------------------------------------------------------------------------------------------

//PRESS BUT BALANCE
$("body").on("click", "button.balance", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(6);
    console.log(id);
    let date = new Date();
    console.log(id);

    //date
    let hs = date.getHours();
    let min = date.getMinutes();
    if (min < 10 ){
        _min = "0" + JSON.stringify(min);
        min = _min;
        console.log("n_min is: " + _min);
    } else if (min > 10){
        console.log("minutes is more than 10");                            
    }
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let p_date = hs + ":" + min + " | " + day + "/" + month;

    let fetch_raw_id = $(this).parent().parent().parent().attr("id");
    console.log("fetch raw id: " + fetch_raw_id);
    let pac_id = fetch_raw_id.substr(10); 

    console.log("PAC ID IS: " + pac_id);

    //get pos
    let main_d = $(this).parent().parent().parent();    
    let a = main_d.find(".card");
    console.log("length: " + a.length);
    let b = +a.length + 1;
    console.log(b);
    pos = b;
   

    $(this).parent().parent().parent().append(`
    <div class="card bg-primary border-secondary text-white balance" id="cont_bal_${pos}_${pac_id}">
        <div class="card-header">
        <h5>Balance</h5>
        <p class="contr_date">${p_date}</p>                                                              
        </div>
                                   
        <button class="btn btn-primary peso" value="0" id="bal_pes_${pos}_${pac_id}">Peso 0 Kg</button>
        <br>
        <div class="card-header">
        <h5>Ingresos</h5>   
        </div>                                                                    
        <h6>VO</h6>
    
        <h6 class="ivo_val" id="bal_ivo_${pos}_${pac_id}">0</h6>
        <div class="btn-group balcenter_buts">
        <button class="btn btn-sm btn-primary ivo50" id="bal_ivo_50_${pos}_${pac_id}">50</button>
        <button class="btn btn-sm btn-primary ivo200" id="bal_ivo_200_${pos}_${pac_id}">200</button>
        <button class="btn btn-sm btn-primary ivo500" id="bal_ivo_500_${pos}_${pac_id}">500</button>
        </div>
        <br>
    
        <h6>EV</h6>
        <h6 id="bal_iev_${pos}_${pac_id}">0</h6>
        <div class="btn-group balcenter_buts">
        <button class="btn btn-sm btn-primary iev50" id="bal_iev_50_${pos}_${pac_id}">50</button>
        <button class="btn btn-sm btn-primary iev200" id="bal_iev_200_${pos}_${pac_id}">200</button>
        <button class="btn btn-sm btn-primary iev500" id="bal_iev_500_${pos}_${pac_id}">500</button>
        </div>
        <br>
        <div class="card-header">
        <h5>Egresos</h5>         
        </div>                         
        <h6>Diuresis</h6>
        <h6 id="bal_diu_${pos}_${pac_id}">0</h6>
        <div class="btn-group balcenter_buts">
        <button class="btn btn-sm btn-primary diu50" id="bal_diu_50_${pos}_${pac_id}">50</button>
        <button class="btn btn-sm btn-primary diu200" id="bal_diu_200_${pos}_${pac_id}">200</button>
        <button class="btn btn-sm btn-primary diu500" id="bal_diu_500_${pos}_${pac_id}">500</button>
        </div>
        <br>
    
        <h6>Catarsis</h6>
        <h6 id="bal_cat_${pos}_${pac_id}">0</h6>
        <div class="btn-group balcenter_buts">
        <button class="btn btn-sm btn-primary catsum" id="bal_cat_sum_${pos}_${pac_id}">+1</button>
        <button class="btn btn-sm btn-primary catrest" id="bal_cat_res_${pos}_${pac_id}">-1</button>                            
        </div>
        <br>
        <button class="btn btn-block btn-sm btn-dark" id="bal_main_${pos}_${pac_id}" disabled >0</button>  
        <button class="btn btn-sm btn-primary float-right save_balance" id="bal_save_${pos}_${pac_id}">Calcular</button>                               
    </div><br>
    `);

    let found_index = get_index_fromid(id);

    camas[found_index].controles.push({type: "balance", date: date, ivo: 0, iev: 0, diu: 0, cat: 0, peso: 0, bal: 0, id: id});

    save_camas();
    
    $(this).hide();
});



//PESO
$("body").on("click", "button.peso", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(9);
    let pos = id_raw.substring(8,9);
    let pesoval = prompt("Ingresar peso en Kg");
    $(this).html(`Peso ${pesoval} Kg`);
    $(this).val(pesoval);

   let found_index = get_index_fromid(id);

   camas[found_index].controles[+pos-1].peso = pesoval;

   save_camas();

});

//ivo 50
$("body").on("click", "button.ivo50", function(){
    let id_raw = $(this).attr("id");
    console.log("id raw: " + id_raw);
    let id = id_raw.substr(12);
    console.log("id: " + id);
    let pos = id_raw.substring(11,12);
    console.log("pos: " + pos);
    let ivo_value = $("#bal_ivo_" + pos + id).html();
    console.log(ivo_value);
    let sum = +ivo_value + 50;
    $("#bal_ivo_" + pos + id).html(sum);
    console.log("#bal_ivo_" + pos + id);
    let ivo_finalval = $("#bal_ivo_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].ivo = ivo_finalval;

    save_camas();
});
//ivo 200
$("body").on("click", "button.ivo200", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(13);
    let pos = id_raw.substring(12,13);
    let ivo_value = $("#bal_ivo_" + pos + id).html();
    let sum = +ivo_value + 200;
    $("#bal_ivo_" + pos + id).html(sum);
    let ivo_finalval = $("#bal_ivo_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].ivo = ivo_finalval;

    save_camas();
});
//ivo 500
$("body").on("click", "button.ivo500", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(13);
    let pos = id_raw.substring(12,13);
    let ivo_value = $("#bal_ivo_" + pos + id).html();
    let sum = +ivo_value + 500;
    $("#bal_ivo_" + pos + id).html(sum);
    let ivo_finalval = $("#bal_ivo_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].ivo = ivo_finalval;

    save_camas();
});
//iev 50
$("body").on("click", "button.iev50", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(12);
    let pos = id_raw.substring(11,12);
    let iev_value = $("#bal_iev_" + pos + id).html();
    let sum = +iev_value + 50;
    $("#bal_iev_" + pos + id).html(sum);
    let ivo_finalval = $("#bal_iev_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].iev = ivo_finalval;

    save_camas();
});
//iev 200
$("body").on("click", "button.iev200", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(13);
    let pos = id_raw.substring(12,13);
    let iev_value = $("#bal_iev_" + pos + id).html();
    let sum = +iev_value + 200;
    $("#bal_iev_" + pos + id).html(sum);
    let iev_finalval = $("#bal_iev_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].iev = iev_finalval;

    save_camas();
});
//iev 500
$("body").on("click", "button.iev500", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(13);
    let pos = id_raw.substring(12,13);
    let iev_value = $("#bal_iev_" + pos + id).html();
    let sum = +iev_value + 500;
    $("#bal_iev_" + pos + id).html(sum);
    let iev_finalval = $("#bal_iev_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].iev = iev_finalval;

    save_camas();
});
//diu 50
$("body").on("click", "button.diu50", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(12);
    let pos = id_raw.substring(11,12);
    let diu_value = $("#bal_diu_" + pos + id).html();
    let sum = +diu_value + 50;
    $("#bal_diu_" + pos + id).html(sum);
    let diu_finalval = $("#bal_diu_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].diu = diu_finalval;

    save_camas();
});
//diu 200
$("body").on("click", "button.diu200", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(13);
    let pos = id_raw.substring(12,13);
    let diu_value = $("#bal_diu_" + pos + id).html();
    let sum = +diu_value + 200;
    $("#bal_diu_" + pos + id).html(sum);
    let diu_finalval = $("#bal_diu_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].diu = diu_finalval;

    save_camas();
});
//diu 200
$("body").on("click", "button.diu500", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(13);
    let pos = id_raw.substring(12,13);
    let diu_value = $("#bal_diu_" + pos + id).html();
    let sum = +diu_value + 500;
    $("#bal_diu_" + pos + id).html(sum);
    let diu_finalval = $("#bal_diu_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].diu = diu_finalval;

    save_camas();
});
//catsum
$("body").on("click", "button.catsum", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(13);
    console.log(id);
    let pos = id_raw.substring(12,13);
    let diu_value = $("#bal_cat_" + pos + id).html();
    let sum = +diu_value + 1;
    $("#bal_cat_" + pos + id).html(sum);
    let iev_finalval = $("#bal_cat_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].cat = iev_finalval;

    save_camas();
});
//catrest
$("body").on("click", "button.catrest", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(13);
    console.log(id);
    let pos = id_raw.substring(12,13);
    let diu_value = $("#bal_cat_" + pos + id).html();
    let sum = +diu_value - 1;
    $("#bal_cat_" + pos + id).html(sum);
    let iev_finalval = $("#bal_cat_" + pos + id).html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].cat = iev_finalval;

    save_camas();
});

//save balance
$("body").on("click", "button.save_balance", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(10); 
    let pos = id_raw.substring(9,10);

    if (pos > 0){
        console.log("pos is a number");
        console.log("pos is: " + pos);
    } else {
        console.log("pos is not a number");
        let main_d = $(this).parent().parent();    
        let a = main_d.find(".form-control");
        let b = +a.length + 1;
        pos = b;
    }
    console.log("POS is: " + pos);

    let ivo_dat = $("#bal_ivo_" + pos + id).html();
    if (ivo_dat === undefined){
        ivo_dat = $("#bal_ivo_" + "x" + id).html();
    } else {}
    console.log(ivo_dat);
    let iev_dat = $("#bal_iev_" + pos + id).html();
    if (iev_dat === undefined){
        iev_dat = $("#bal_iev_" + "x" + id).html();
    } else {}
    console.log(iev_dat);
    let diu_dat = $("#bal_diu_" + pos + id).html();
    if (diu_dat === undefined){
        diu_dat = $("#bal_diu_" + "x" + id).html();
    } else {}
    console.log(diu_dat);
    let cat_dat = $("#bal_cat_" + pos + id).html();
    if (cat_dat === undefined){
        cat_dat = $("#bal_cat_" + "x" + id).html();
    } else {}
    console.log(cat_dat);
    let pes_dat = $("#bal_pes_" + pos + id).val();
    if (pes_dat === undefined){
        pes_dat = $("#bal_pes_" + "x" + id).val();
    } else {}
    console.log(pes_dat);
    

    //math
    let in_subt = +ivo_dat + +iev_dat;
    let out_subt = +diu_dat + +cat_dat * 200;
    console.log("in: " + in_subt);
    console.log("out: " + out_subt);
    let p_ins = +pes_dat / 2 * 24;
    console.log(p_ins)
    let agua_e = +pes_dat * 5;
    console.log(agua_e);

    let in_tot = +in_subt + agua_e;
    let out_tot = +out_subt + p_ins;
    let total = +in_tot - +out_tot;

    let bal = total;

    let write_bal = $("#bal_main_" + pos + id);
    console.log(write_bal.html());
    if (write_bal.html() === undefined){
        write_bal = $("#bal_main_" + "x" + id);  
    } else {}

    console.log(write_bal.html());

    write_bal.html(total);

   
    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].bal = total;

    save_camas();
   
});

//-----------------------------------------------------------------------------------------------------
//PRESS BUT CONTROL HGT
//------------------------------------------------------------------------------------------------------

$("body").on("click", "button.hgt", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(6);
    console.log(id);
    let date = new Date();
    console.log(id);

    //date
    let hs = date.getHours();
    let min = date.getMinutes();
    if (min < 10 ){
        _min = "0" + JSON.stringify(min);
        min = _min;
        console.log("n_min is: " + _min);
    } else if (min > 10){
        console.log("minutes is more than 10");                            
    }
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let p_date = hs + ":" + min + " | " + day + "/" + month;

    let fetch_raw_id = $(this).parent().parent().parent().attr("id");
    let pac_id = fetch_raw_id.substr(10); 


    let found_index = get_index_fromid(id);

    camas[found_index].controles.push({type: "hgt", date: date, pd: 0, pa: 0, pm: 0, pc: 0, id: id});

    save_camas();

    //get pos
    let main_d = $(this).parent().parent().parent();    
    let a = main_d.find(".card");
    console.log("length: " + a.length);
    let b = +a.length + 1;
    console.log(b);
    pos = b;

   $(this).parent().parent().parent().append(`
    <div class="card bg-primary border-secondary text-white" id="hgt">
    <div class="card-header">
    <h5>HGT</h5>
    <p>${p_date}</p>
    </div>
    <div class="btn-group balcenter_buts">
    <button class="btn btn-primary hgt_d" id="hgt_d_${pos}_${pac_id}">D:</button>
    <button class="btn btn-primary hgt_a" id="hgt_a_${pos}_${pac_id}">A:</button>
    <button class="btn btn-primary hgt_m" id="hgt_m_${pos}_${pac_id}">M:</button>
    <button class="btn btn-primary hgt_c" id="hgt_c_${pos}_${pac_id}">C:</button>
    </div>
    </div><br>`);

    $(this).hide();
});

$("body").on("click", "button.hgt_d", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(8); 
    let pos = id_raw.substring(6,7);

    let hgt_d_val = prompt("Ingresar Hgt Desayuno");
    $(this).html(`D: ${hgt_d_val}`);
    $(this).val(hgt_d_val);

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].pd = hgt_d_val;

    save_camas();
});
$("body").on("click", "button.hgt_a", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(8); 
    let pos = id_raw.substring(6,7);

    let hgt_a_val = prompt("Ingresar Hgt Almuerzo");
    $(this).html(`A: ${hgt_a_val}`);
    $(this).val(hgt_a_val);
    
    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].pa = hgt_a_val;

    save_camas();
});
$("body").on("click", "button.hgt_m", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(8); 
    let pos = id_raw.substring(6,7);

    let hgt_m_val = prompt("Ingresar Hgt Merienda");
    $(this).html(`M: ${hgt_m_val}`);
    $(this).val(hgt_m_val);
    
    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].pm = hgt_m_val;

    save_camas();
});
$("body").on("click", "button.hgt_c", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(8); 
    let pos = id_raw.substring(6,7);

    let hgt_c_val = prompt("Ingresar Hgt Cena");
    $(this).html(`C: ${hgt_c_val}`);
    $(this).val(hgt_c_val);

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].pc = hgt_c_val;

    save_camas();
});

//-----------------------------------------------------------------------------------------------------
//PRESS BUT CONTROL RITDIU
//------------------------------------------------------------------------------------------------------

$("body").on("click", "button.ritdiu", function(){

    let id_raw = $(this).attr("id");
    let id = id_raw.substr(6);
    console.log(id_raw);
    console.log(id);
    let date = new Date();
    //date
    let hs = date.getHours();
    let min = date.getMinutes();
    if (min < 10 ){
        _min = "0" + JSON.stringify(min);
        min = _min;
        console.log("n_min is: " + _min);
    } else if (min > 10){
        console.log("minutes is more than 10");                            
    }
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let p_date = hs + ":" + min + " | " + day + "/" + month;

    let fetch_raw_id = $(this).parent().parent().parent().attr("id");
    let pac_id = fetch_raw_id.substr(10);
    console.log(pac_id); 

    //get pos
    let main_d = $(this).parent().parent().parent();    
    let a = main_d.find(".card");
    console.log("length: " + a.length);
    let b = +a.length + 1;
    console.log(b);
    pos = b;

    $(this).parent().parent().parent().append(`
    <div class="card bg-primary text-white border-secondary ritdiu" id="ritdiu_${pos}_${id}">
            <div class="card-header">
            <h5>Ritmo Diuretico</h5>
            <p>${p_date}</p>
            </div>
     
            <div class="card-header">
            <h5>Inicio de Seguimiento</h6>
            </div>
            <h6>Hora y Dia de inicio</h6>
            <div class="btn-group balcenter_buts">
            <button class="btn btn-sm btn-primary ritdiu_hs" style="min-width: 50px;" >00</button>
            <button class="btn btn-sm btn-dark" disabled>:</button>
            <button class="btn btn-sm btn-primary ritdiu_mins" style="min-width: 50px;" >00</button>
            </div>
            <br>
            <p id="ritdiu_init_date">No definida</p>
            <div class="btn-group balcenter_buts" >
            <button class="btn btn-sm btn-primary ritdiu_hoy" id="ritdiu_${pos}_${id}">Hoy</button>
            <button class="btn btn-sm btn-primary ritdiu_ayer" id="ritdiu_${pos}_${id}">Ayer</button>
            </div>
            <br>
            <h6>Diuresis de inicio en ml</h6>
            <button class="btn btn-primary btn-sm ritdiu_initdiu" id="ritdiu_${pos}_${id}">0</button>
            <div class="card-header">
            <h5>Ingresar Diuresis</h5>
            </div>
            <p id="ritdiu_${pos}_${id}" value="0" class="ritdiu_newdiu">0</p>
            <div class="btn-group balcenter_buts">
            <button class="btn btn-sm btn-primary ritdiu_50" id="ritdiu_${pos}_${id}">+50</button>
            <button class="btn btn-sm btn-primary ritdiu_200" id="ritdiu_${pos}_${id}">+200</button>
            <button class="btn btn-sm btn-primary ritdiu_500" id="ritdiu_${pos}_${id}">+500</button>
            </div>
            <br> 
            <button class="btn btn-block btn-sm btn-dark ritdiu_final" disabled>ml/hs</button>
            <button class="btn btn-block btn-primary ritdiu_calc" id="ritdiu_${pos}_${id}">Calcular</button>                              
            </div>
            <br>
    `);


    let found_index = get_index_fromid(id);

    camas[found_index].controles.push({type: "ritdiu", date: date, init_date: 0, init_diu: 0, current_diu: 0, id: id});

    save_camas();
});
//hs
$("body").on("click", "button.ritdiu_hs", function(){
    let id_raw = $(this).attr("id");
    let main_d = $(this).parent().parent().parent();  
    let find = $(main_d).find(".ritdiu");
    let grabbed_id = find.attr("id");
    let id = grabbed_id.substr(6);
    console.log(find.attr("id"));
    console.log(id);

    let set_hs = prompt("Ingresar Hora");
    $(this).html(set_hs);
});
//mins
$("body").on("click", "button.ritdiu_mins", function(){
    let id_raw = $(this).attr("id");
    let main_d = $(this).parent().parent().parent();  
    let find = $(main_d).find(".ritdiu");
    let grabbed_id = find.attr("id");
    let id = grabbed_id.substr(6);
    console.log(find.attr("id"));
    console.log(id);

    let set_mins = prompt("Ingresar Minutos");
    $(this).html(set_mins);
});

//hoy
$("body").on("click", "button.ritdiu_hoy", function(){
    let id_raw = $(this).attr("id");
    console.log(id_raw);
    let main_d = $(this).parent().parent().parent();  
    let find = $(main_d).find(".ritdiu");
    let grabbed_id = find.attr("id");
    let id = grabbed_id.substr(6);
    console.log(find.attr("id"));
    console.log(id);

    //get date
    let raw_setdate = new Date();
    //date
    let year = raw_setdate.getFullYear();
    let month = raw_setdate.getMonth() + 1;
    let raw_month = raw_setdate.getMonth();
    let day = raw_setdate.getDate();
    console.log(day);
    let format_date = day + "/" + month;

    
    let pos = id_raw.substring(7,8);
    console.log("POS is: " + pos)
    if (pos > 0){
        console.log("pos is a number");
    } else {
        console.log("pos is not a number");
        let a = main_d.find(".form-control");
        let b = a.length;
        console.log(b);
        pos = +b+1;
    }

    console.log("POS: " + pos);

    let found_initdate_display =  $(this).parent().parent().find("#ritdiu_init_date");
    found_initdate_display.html(format_date);

    //get hs and mins
    let get_hs = $(this).parent().parent().find(".ritdiu_hs").html();
    let get_mins = $(this).parent().parent().find(".ritdiu_mins").html();
    let get_completetime = get_hs + ":" + get_mins;

    let date_to_send = new Date(year, raw_month, day, get_hs, get_mins)
    console.log("Date to send: " + date_to_send);

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].init_date = date_to_send;

    save_camas();


    $(this).parent().hide();
    $(this).parent().parent().find(".ritdiu_hs").attr("disabled", "disabled");
    $(this).parent().parent().find(".ritdiu_mins").attr("disabled", "disabled");
});
//ayer
$("body").on("click", "button.ritdiu_ayer", function(){
    let id_raw = $(this).attr("id");
    console.log(id_raw);
    let main_d = $(this).parent().parent().parent();  
    let find = $(main_d).find(".ritdiu");
    let grabbed_id = find.attr("id");
    let id = grabbed_id.substr(6);
    console.log(find.attr("id"));
    console.log(id);

    //get date
    let raw_setdate = new Date();
    //date
    let year = raw_setdate.getFullYear();
    let month = raw_setdate.getMonth() + 1;
    let raw_month = raw_setdate.getMonth();
    let day = raw_setdate.getDate() - 1;
    console.log(day);
    let format_date = day + "/" + month;

    
    let pos = id_raw.substring(7,8);
    console.log("POS is: " + pos)
    if (pos > 0){
        console.log("pos is a number");
    } else {
        console.log("pos is not a number");
        let a = main_d.find(".form-control");
        let b = a.length;
        console.log(b);
        pos = +b+1;
    }

   

    let found_initdate_display =  $(this).parent().parent().find("#ritdiu_init_date");
    found_initdate_display.html(format_date);

    //get hs and mins
    let get_hs = $(this).parent().parent().find(".ritdiu_hs").html();
    let get_mins = $(this).parent().parent().find(".ritdiu_mins").html();
    let get_completetime = get_hs + ":" + get_mins;

    let date_to_send = new Date(year, raw_month, day, get_hs, get_mins)
    console.log("Date to send: " + date_to_send);

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].init_date = date_to_send;

    save_camas();

    $(this).parent().hide();
    $(this).parent().parent().find(".ritdiu_hs").attr("disabled", "disabled");
    $(this).parent().parent().find(".ritdiu_mins").attr("disabled", "disabled");
});
//diuresis de inicio
$("body").on("click", "button.ritdiu_initdiu", function(){
    let id_raw = $(this).attr("id");
    console.log(id_raw);
    let main_d = $(this).parent().parent().parent();  
    let find = $(main_d).find(".ritdiu");
    let grabbed_id = find.attr("id");
    let id = grabbed_id.substr(6);
    console.log(find.attr("id"));
    console.log(id);

    let pos = id_raw.substring(7,8);
    if (pos > 0){
        console.log("pos is a number");
    } else {
        console.log("pos is not a number");
        let a = main_d.find(".form-control");
        let b = a.length;
        console.log(b);
        pos = b;
        console.log("if pos: " + pos);
    }
    console.log("POS is: " + pos);

    let input_init_diu = prompt("Ingresar diuresis de ingreso");
    $(this).html(input_init_diu);

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].init_diu = input_init_diu;

    save_camas(); 
});
//ritdiu 50 
$("body").on("click", "button.ritdiu_50", function(){
    let id_raw = $(this).attr("id");
    console.log(id_raw);
    let main_d = $(this).parent().parent().parent();  
    let find = $(main_d).find(".ritdiu");
    let grabbed_id = find.attr("id");
    let id = grabbed_id.substr(6);
    console.log(find.attr("id"));
    console.log(id);

    let pos = id_raw.substring(7,8);
    if (pos > 0){
        console.log("pos is a number");
    } else {
        console.log("pos is not a number");
        let a = main_d.find(".form-control");
        let b = a.length;
        console.log(b);
        pos = +b+1;
    }
    console.log("POS is: " + pos);

    let get_parentdiv = $(this).parent().parent();
    let get_currentdiu = get_parentdiv.find(".ritdiu_newdiu").html();
    console.log(get_currentdiu);
    let sum = +get_currentdiu + 50;
    $(this).parent().parent().find(".ritdiu_newdiu").html(sum);
    let send_diu =   $(this).parent().parent().find(".ritdiu_newdiu").html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].current_diu = send_diu;

    save_camas(); 
});
//ritdiu 200 
$("body").on("click", "button.ritdiu_200", function(){
    let id_raw = $(this).attr("id");
    console.log(id_raw);
    let main_d = $(this).parent().parent().parent();  
    let find = $(main_d).find(".ritdiu");
    let grabbed_id = find.attr("id");
    let id = grabbed_id.substr(6);
    console.log(find.attr("id"));
    console.log(id);

    let pos = id_raw.substring(7,8);
    if (pos > 0){
        console.log("pos is a number");
    } else {
        console.log("pos is not a number");
        let a = main_d.find(".form-control");
        let b = a.length;
        console.log(b);
        pos = +b+1;
    }
    console.log("POS is: " + pos);

    let get_parentdiv = $(this).parent().parent();
    let get_currentdiu = get_parentdiv.find(".ritdiu_newdiu").html();
    console.log(get_currentdiu);
    let sum = +get_currentdiu + 200;
    $(this).parent().parent().find(".ritdiu_newdiu").html(sum);
    let send_diu =   $(this).parent().parent().find(".ritdiu_newdiu").html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].current_diu = send_diu;

    save_camas(); 
});
//ritdiu 500
$("body").on("click", "button.ritdiu_500", function(){
    let id_raw = $(this).attr("id");
    console.log(id_raw);
    let main_d = $(this).parent().parent().parent();  
    let find = $(main_d).find(".ritdiu");
    let grabbed_id = find.attr("id");
    let id = grabbed_id.substr(6);
    console.log(find.attr("id"));
    console.log(id);

    let pos = id_raw.substring(7,8);
    if (pos > 0){
        console.log("pos is a number");
    } else {
        console.log("pos is not a number");
        let a = main_d.find(".form-control");
        let b = a.length;
        console.log(b);
        pos = +b+1;
    }
    console.log("POS is: " + pos);

    let get_parentdiv = $(this).parent().parent();
    let get_currentdiu = get_parentdiv.find(".ritdiu_newdiu").html();
    console.log(get_currentdiu);
    let sum = +get_currentdiu + 500;
    $(this).parent().parent().find(".ritdiu_newdiu").html(sum);
    let send_diu =   $(this).parent().parent().find(".ritdiu_newdiu").html();

    let found_index = get_index_fromid(id);

    camas[found_index].controles[+pos-1].current_diu = send_diu;

    save_camas(); 
});
//calc
$("body").on("click", "button.ritdiu_calc", function(){
    let id_raw = $(this).attr("id");
    console.log(id_raw);
    let main_d = $(this).parent().parent().parent();  
    let find = $(main_d).find(".ritdiu");
    let grabbed_id = find.attr("id");
    let id = grabbed_id.substr(6);
    console.log(find.attr("id"));
    console.log(id);

    let pos = id_raw.substring(7,8);
    if (pos > 0){
        console.log("pos is a number");
    } else {
        console.log("pos is not a number");
        let a = main_d.find(".form-control");
        let b = a.length;
        console.log(b);
        pos = b;
    }
    console.log("POS is: " + pos);

    let calc_ritdiu
    let target_dom = $(this).parent().parent().find(".ritdiu_final");

    let found_index = get_index_fromid(id);

    let ridtiu_fulldata = camas[found_index].controles[+pos-1];
    console.log(ridtiu_fulldata);
    let init_date = ridtiu_fulldata.init_date;
    console.log("init_date: " + init_date);
    let init_diu = ridtiu_fulldata.init_diu;
    let current_diu = ridtiu_fulldata.current_diu;

    let now_date = new Date();
    let now_ms = now_date.getTime();
    console.log("now: " + now_date);
    console.log("now ms: " + now_ms);

    let init_date_formatted = new Date(init_date);
    let init_date_ms = init_date_formatted.getTime();
    console.log("stored: " + init_date_formatted);
    console.log("stored ms: " + init_date_ms);

    let time_diff = parseInt(now_ms) - parseInt(init_date_ms);
    console.log(time_diff + " ms");
    let time_diff_hs_raw = +time_diff/3600000;
    let time_diff_hs= time_diff_hs_raw.toFixed(0);

    let diu_diff = current_diu - init_diu;
    console.log("DIFF: " + diu_diff);

    calc_ritdiu_raw = +diu_diff / +time_diff_hs;
    calc_ritdiu = calc_ritdiu_raw.toFixed(0);
    console.log(calc_ritdiu);
    target_dom.html(calc_ritdiu + " ml/min");
   
});

//ver button
$("body").on("click", "button.ver_but", function(){
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(3);
    console.log(id); 
    let controles_div = $("#controles" + id);
    controles_div.toggle("fast");    
});

//DELETE PATIENT
$("body").on("click", "button.delete_pac", function(){
    alert("borrar paciente?");
    let id_raw = $(this).attr("id");
    let id = id_raw.substr(7);
    console.log(id);

    let found_index = get_index_fromid(id);

    camas.splice(found_index, 1);
    console.log("spliced");

    save_camas();

    $(`#mainpac${id}`).remove();
    $(`#br${id}`).remove();

});

$("body").on("click", "button.change_cama", function(){

    let id = $(this).attr("id");
    console.log("this button id is: " + id);

    let id_nombre = $("#" + id + ".change_nombre").html();
    console.log("nombre id: " + id_nombre);
    let id_edad = $("#" + id + ".change_edad").html();
    console.log("edad id: " + id_edad);

    let val = prompt("Nueva Cama");

    $(this).html(val);
  
    let new_id = `_${val}${id_nombre}${id_edad}`;
    console.log("new id is: " + new_id)

    $(this).attr("id", `_${val}${id_nombre}${id_edad}`);
    console.log(`_${val}${id_nombre}${id_edad}`)
    $(this).parent().parent().attr("id", `mainpac_${val}${id_nombre}${id_edad}`);

    let found_index = get_index_fromid(id);

    console.log("found index is: " + found_index);

    camas[found_index].numero = val;

    camas[found_index].id = new_id;

    let pendientes_length = camas[found_index].pendientes.length;

    for (let p = 0; p < pendientes_length; p++){
        camas[found_index].pendientes[p].id = new_id;
    }

    save_camas();

    location.reload();

});

document.getElementById("ayuda").addEventListener("click", function () {

    window.location.href = "ayuda.html";

});

document.getElementById("herramientas").addEventListener("click", function () {

    window.location.href = "herramientas.html";

});

$("#nuevopase").click(()=>{
    localStorage.clear();
    camas = [];
    save_camas();
    $("#camas_data").empty();
});


//==================================================================================================
//ESTUDIOS PENDIENTES
//==================================================================================================

$("body").on("click", "button.espend_but", function(){

    let main_d = $(this).parent().parent();    
    let grabbed_id = main_d.attr("id");
    console.log("grabbed id is: " + grabbed_id);
    let clean_id = grabbed_id.substr(9)
    let id = clean_id;
    console.log(id);
    let date = new Date();
    console.log(id);

    //get pos
    let a = main_d.find(".pendiente-card");
    //console.log("length: " + a.length);
    let b = +a.length + 1;
    //console.log(b);
    pos = b;

    let tit = prompt("Estudio Pendiente:");

    let found_index = get_index_fromid(id);

    console.log("found index is: " + found_index);

    //modify camas and save to localstorage

    camas[found_index].pendientes.push({titulo: tit, status: "border-danger", id: id})

    save_camas();


    $(this).parent().parent().find(".estudios").append(`
    <br id="${pos}_${id}">
    <div class="card bg-primary text-white hemodinamico border-danger pendiente-card" id="pendientes_${pos}${id}">
        <div class="card-header">
        <button class="btn btn-sm btn-danger float-left delete_pendiente">x</button>                                
        <h6>${tit}</h6>
        <hr style="border-color: #BEEE62">
        <button class="btn btn-sm btn-primary pendiente">Pendiente</button>
        <button class="btn btn-sm btn-primary encurso">En Curso</button>
        <button class="btn btn-sm btn-primary hecho">Hecho</button>
        </div>                                        
    </div>
    `);
});

$("body").on("click", "button.encurso", function(){
    $(this).parent().parent().attr("class", "card bg-primary text-white hemodinamico border-warning pendiente-card");

    let main_d = $(this).parent().parent().parent();
    let raw_pos = $(this).parent().parent().attr("id");
    console.log("rawpos: " + raw_pos)
    let pos = raw_pos.substring(11,12);
    console.log("pos:" + pos);
    let grabbed_id = main_d.attr("id");
    console.log("grabbed id is: " + grabbed_id);
    let id = grabbed_id;
    console.log(id);
    let date = new Date();
    console.log(id);

    let found_index = get_index_fromid(id);

    camas[found_index].pendientes[+pos - 1].status = "border-warning";

    save_camas();
});
$("body").on("click", "button.pendiente", function(){
    $(this).parent().parent().attr("class", "card bg-primary text-white hemodinamico border-danger pendiente-card");

    let main_d = $(this).parent().parent().parent();
    let raw_pos = $(this).parent().parent().attr("id");
    console.log("rawpos: " + raw_pos)
    let pos = raw_pos.substring(11,12);
    console.log("pos:" + pos);
    let grabbed_id = main_d.attr("id");
    console.log("grabbed id is: " + grabbed_id);
    let id = grabbed_id;
    console.log(id);
    let date = new Date();
    console.log(id);

    let found_index = get_index_fromid(id);

    camas[found_index].pendientes[+pos - 1].status = "border-danger";

    save_camas();
});
$("body").on("click", "button.hecho", function(){
    $(this).parent().parent().attr("class", "card bg-primary text-white hemodinamico border-success pendiente-card");

    let main_d = $(this).parent().parent().parent();
    let raw_pos = $(this).parent().parent().attr("id");
    console.log("rawpos: " + raw_pos)
    let pos = raw_pos.substring(11,12);
    console.log("pos:" + pos);
    let grabbed_id = main_d.attr("id");
    console.log("grabbed id is: " + grabbed_id);
    let id = grabbed_id;
    console.log(id);
    let date = new Date();
    console.log(id);

    let found_index = get_index_fromid(id);

    camas[found_index].pendientes[+pos - 1].status = "border-success";

    save_camas();
});

$("body").on("click", "button.delete_pendiente", function(){
    console.log("PRESS DELETE");
    let main_d = $(this).parent().parent().parent();
    let raw_pos = $(this).parent().parent().attr("id");
    console.log("rawpos: " + raw_pos)
    let pos = raw_pos.substring(11,12);
    console.log("pos:" + pos);
    let grabbed_id = main_d.attr("id");
    console.log("grabbed id is: " + grabbed_id);
    let id = grabbed_id;
    console.log(id);
    let date = new Date();
    console.log(id);

    let found_index = get_index_fromid(id);

    camas[found_index].pendientes.splice(+pos - 1, 1);

    save_camas();
    
    $(this).parent().parent().parent().find("br#" + pos + "_" + grabbed_id).remove();
    $(this).parent().parent().remove();
});




